function q = modularity(A, c, varargin)
%MODULARITY calculate overlapping Q for a network
%   Q = MODULARITY(A, C) return Newman's modularity, Q, for a network
%   defined by an adjacency matrix A and a list of communities C. C
%   can either be a 2 column list of nodes and communities or a cover.
%
%   Q = MODULARITY(A, C, 'TYPE', TYPE, ...) specify whether to use
%   Newman's modularity ('newman'), the modularity density version
%   ('density'), or 'both' to calculate both.  If both, returns a
%   vector [newman, density]. Defaults to Newman's.
%
%   Q = MODULARITY(A, C, 'BELONGING', FUNC, ...) set which belonging
%   function to use. Options are 'product' (default) or 'average'.
%   Modularity definitions from "Extension of Modularity Density for
%   Overlapping Communitiy Structure" by Chen, Kusmin and Szymanski.
%
%   Q = MODULARITY(A, C, 'USEMEX', BOOL, ...) if available, use the
%   mex function for calculating edges. When false force use of matlab
%   version.

    A = getAdj(A);
    c = getCover(c);

    assert(size(A, 1) == size(A, 2), 'Adjacency matrix is not square.');

    missingNodes = length(A) - size(c, 1);
    if missingNodes > 0
        warning('MATLAB:modularity:MissingNodes', ...
                ['Adjacency matrix and cover have different numbers of nodes. ', ...
                 'Assuming last nodes not assigned to a community.']);
        c = [c; zeros(missingNodes, size(c, 2))];
    elseif missingNodes < 0 && issparse(A)
        warning('MATLAB:modularity:MissingNodes', ...
                ['Adjacency matrix and cover have different numbers of nodes. ', ...
                 'Assuming last nodes have no connections.']);

        [i, j, s] = find(A);
        A = sparse(i, j, s, size(c, 1), size(c, 1));
        clear('i', 'j', 's');
    elseif missingNodes < 0 % && ~issparse(A)
         % If A is not sparse it was read in as a full matrix . Under
         % this case, assume the number of columns/rows represents the
         % true number of nodes and this is actually an error.
        error('MATLAB:modularity:MissingNodes', ...
              'Adjacency matrix and cover have different numbers of nodes');
    end

    p = inputParser;
    allowedTypes = {'newman', 'density', 'both'};
    allowedBelongingFunctions = {'product', 'average'};
    testParameter = @(x, allowedValues) any(validatestring(x, allowedValues));
    p.addParameter('type', 'newman', @(x) testParameter(x, allowedTypes));
    p.addParameter('belonging', 'product', ...
                   @(x) testParameter(x, allowedBelongingFunctions));
    p.addParameter('useMex', true, @islogical);
    p.parse(varargin{:});
    belonging = validatestring(p.Results.belonging, allowedBelongingFunctions);
    type = validatestring(p.Results.type, allowedTypes);

    if p.Results.useMex && (exist('modularityMex', 'file') == 3)
        if ~isnumeric(A)
            A = double(A);
        end
        if ~isnumeric(c)
            c = double(c);
        end
        q = modularityMex(A, c, belonging);

        switch type
          case 'newman'
            q = q(1);
          case 'density'
            q = q(2);
            % otherwise q = q;
        end
    else
        if p.Results.useMex
            warning(['MEX version of MODULARITY not compiled; using M ', ...
                     'function instead. To take advantage of the mex version', ...
                     'run this package''s MAKE command (see README for', ...
                     'more information).']);
        end

        a = c ./ sum(c, 2);
        edgesPairwise = countEdges(a, A, belonging);
        edgesIn = diag(edgesPairwise)';
        edgesOut = sum(edgesPairwise, 1) - edgesIn;
        edgesIn = edgesIn / 2;
        edges = sum(A, 'all') / 2;

        switch type
          case 'newman'
            q = newmanModularity();
          case 'density'
            q = modularityDensity();
          case 'both'
            newman = newmanModularity();
            density = modularityDensity();
            q = [newman, density];
        end
    end

    function q = newmanModularity()
        q = sum((edgesIn ./ edges) - ...
                ((((2 * edgesIn) + edgesOut) ./ (2 * edges)) .^ 2));
    end

    function q = modularityDensity()
        cSize = sum(a, 1);
        dcDenominator = cSize .* (cSize - 1);
        dcDenominator(dcDenominator == 0) = 1;
        dc = (2 * edgesIn) ./ dcDenominator;
        dccDenominator = cSize' * cSize;
        dcc = edgesPairwise ./ dccDenominator;
        q = sum((edgesIn .* dc / edges) - ...
                ((dc .* ((2 * edgesIn) + edgesOut) / (2 * edges)) .^ 2) - ...
                sum(notDiag(edgesPairwise) .* notDiag(dcc) / (2 * edges), 1));

        function mat = notDiag(mat)
            mat = mat - diag(diag(mat));
        end
    end
end
