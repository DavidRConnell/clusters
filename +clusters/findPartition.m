function c = findPartition(A, varargin)
%FINDPARTITION identify communities in a graph

    A = getAdj(A);
    p = inputParser;
    p.KeepUnmatched = true; % To forward method specific variables.

    allowedTypes = {'leiden', 'louvain', 'slpa'};
    testParameter = @(x, allowedValues) any(validatestring(x, allowedValues));
    p.addParameter('type', 'leiden', @(x) testParameter(x, allowedTypes));
    p.parse(varargin{:});

    switch validatestring(p.Results.type, allowedTypes)
      case 'leiden'
        c = leiden(A, varargin{:});
      case 'louvain'
        c = louvain(A, varargin{:});
      case 'slpa'
        c = slpa(A, varargin{:});
        [~, c] = find(getCover(c));
    end
end