function make()
%MAKE compile all mex files in package
%   MAKE() compile mex files. If any files have already been compiled
%   they will be recompiled only if the source code has been edited
%   since last compilation.

    oldDir = pwd;
    cdPackage();
    try
        compileMexFiles();
    catch ME
        cd(oldDir);
        rethrow(ME);
    end
    cd(oldDir);

    function cdPackage()
        filepath = mfilename('fullpath');
        packageRoot = fileparts(filepath);
        cd(packageRoot);
    end

    function compileMexFiles()
        outDir = 'private';
        mexDir = '../mex';

        isCompiled = @(name) exist(fullfile(pwd, outDir, [name '.' mexext]), 'file') == 2;
        arch = computer('arch');

        if ~isCompiled('modularityMex') || isStale('modularityMex')
            switch arch
              case {'glnxa64', 'maci64'}
                mex CFLAGS='$CFLAGS -fopenmp' ...
                    LDFLAGS='$LDFLAGS -fopenmp' ...
                    COPTIMFLAGS='-O3 -fwrapv -DNDEBUG' ...
                    ../mex/modularityMex.c -outdir private
              case 'win64'
                mex COMPFLAGS='$COMPFLAGS /openmp' ...
                    COPTIMFLAGS='-O3 -fwrapv -DNDEBUG' ...
                    ../mex/modularityMex.c -outdir private
            end
        end

        if ~isCompiled('trilList') || isStale('trilList')
            mex COPTIMFLAGS='-O3 -fwrapv -DNDEBUG' ...
                ../mex/trilList.c -outdir private
        end

        if ~isCompiled('countUnique') || isStale('countUnique')
            mex COPTIMFLAGS='-O3 -fwrapv -DNDEBUG' ...
                ../mex/countUnique.c -outdir private
        end

        if ~isCompiled('leidenMex') || isStale('leidenMex')
            mex COPTIMFLAGS='-O3 -fwrapv' ../mex/leidenMex.c -ligraph  -outdir private
        end

        if ~isCompiled('louvainMex') || isStale('louvainMex')
            mex COPTIMFLAGS='-O3 -fwrapv' ../mex/louvainMex.c -ligraph  -outdir private
        end

        function TF = isStale(name)
            mexFile = dir(fullfile(pwd, outDir, [name '.' mexext]));
            cFile = dir(fullfile(pwd, mexDir, [name '.c']));

            TF = mexFile.datenum < cFile.datenum;
        end
    end
end