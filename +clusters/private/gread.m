function data = gread(filename)
    %GREAD read contents of a file as a graph element

    ext = split(filename, '.');
    switch ext{end}
      case 'groups'
        data = readGroup(filename);
      case {'txt', 'csv'}
        delimPattern = '(\d)(\D)?';
        fid = fopen(filename, 'r');
        l = fgetl(fid);
        c = 0;
        while isheader(l)
            l = fgetl(fid);
            c = c + 1;
        end
        fclose(fid);
        data = dlmread(filename, delim(l), c, 0);
    end

    function TF = isheader(l)
        data = regexp(l, delimPattern, 'tokens', 'once');
        TF = isempty(data);
    end

    function d = delim(l)
        data = regexp(l, delimPattern, 'tokens', 'once');
        d = data{2};
    end
end