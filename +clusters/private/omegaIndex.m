function score = omegaIndex(c1, c2)
%OMEGAINDEX calculate the omegaIndex between two covers
%   SCORE = OMEGAINDEX(C1, C2) calculates the omegaIndex from Collins
%   and Dent 1988 between covers C1 and C2.

    M = nNodePairs(c1, c2);
    t1 = communityOverlap(c1);
    t2 = communityOverlap(c2);

    wu = unadjustedOmega(t1, t2, M);
    we = expectedOmega(t1, t2, M);
    score = full((wu - we) / (1 - we));
end

function M = nNodePairs(c1, c2)
    assert(size(c1, 1) == size(c2, 1), ...
           "Covers do not have the same number of nodes.")

    n = size(c1, 1);
    M = n * (n - 1) / 2;
end

function t = communityOverlap(c)
    t = c * c';
    if exist('trilList', 'file') == 3
        t = trilList(t, -1);
    else
        warning('MEX function TRILLIST not compiled. Use MAKE to compile.');
        t = tril(t + 1, -1) - 1;
        t = sparse(t(t > -1));
    end
end

function wu = unadjustedOmega(t1, t2, M)
    wu = sum(t1 == t2);
    wu = wu / M;
end

function we = expectedOmega(t1, t2, M)
    k = max(max(t1), max(t2));
    if exist('countUnique', 'file') == 3
        t = countUnique(t1, k) .* countUnique(t2, k);
    else
        warning('MEX function COUNTUNIQUE not compiled. Use MAKE to compile.');
        t = sum(t1 == (0:k), 1) .* sum(t2 == (0:k), 1);
    end

    we = sum(t);
    we = we / (M ^ 2);
end