function c = louvain(A, varargin)
%LOUVAIN community detection
%   LOUVAIN(A) perform louvain on the adjacency matrix A.
%   LOUVAIN(A, ..., "resolution", gamma) resolution parameter to use.
%   LOUVAIN(A, ..., "seed", seed) random seed to use if reprehensibility is
%       needed. By default a seed is chosen at random.

    p = inputParser();
    p.KeepUnmatched = true;
    p.addParameter('resolution', 1, @(x) isnumeric(x) && x > 0);
    p.addParameter('seed', randi(10000, 1), @isnumeric);
    p.parse(varargin{:});

    c = louvainMex(A, p.Results.resolution, p.Results.seed, issymmetric(A));
    c = c + 1; % 0 index -> 1 index
end