function c = slpa(A, varargin)
    p = inputParser;
    p.KeepUnmatched = true;
    p.addParameter('r', 0.5, @(x) x >= 0 && x <= 1);
    p.addParameter('T', 500, @(x) x > 0 && rem(x, 1) == 0);
    p.parse(varargin{:});

    A = logical(A);
    memory = eye(size(A));
    nNodes = size(memory, 1);

    for t = 1:p.Results.T
        for i = randperm(nNodes)
            speakers = A(i, :);
            labels = memory(speakers, :);
            labels = speakerRule(labels);
            newLabelIdx = listenerRule(labels);
            memory(i, newLabelIdx) = memory(i, newLabelIdx) + 1;
        end
    end

    c = (memory ./ sum(memory, 2)) > p.Results.r;
    c = [c, sum(c, 2) == 0];
    c = c(:, sum(c, 1) > 0);
end

function labels = speakerRule(labels)
% Pick a label from each speaker's list of seen labels at random
% with probability based on frequency. If a node has seen one label
% once another three times and a third six times, one of those three
% labels will be choosen with probabalities 0.1, 0.3, and 0.6.

    nNeighbors = size(labels, 1);
    freqs = labels ./ sum(labels, 2);
    labels = [zeros(nNeighbors, 1), ...
              cumsum(freqs, 2) > rand(nNeighbors, 1)];
    labels = diff(labels, [], 2);
end

function labelIdx = listenerRule(labels)
% Pick one of the speaker's labels on a majority rule. The label
% that has been presented by the most speakers is selected. If there
% are multiple labels that are presented by the same number of
% speakers, choose one at random.
%
% NOTE: Returns value as an index instead of a vector.

    count = sum(labels, 1);
    labelIdx = find(count == max(count));

    if length(labelIdx) > 1
        labelIdx = labelIdx(randperm(length(labelIdx), 1));
    end
end