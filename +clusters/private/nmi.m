function score = nmi(c1, c2)
%NMI calculate normalized mutual information between two covers
%   SCORE = NMI(C1, C2) computes the normalized mutual information from
%   Lancichinetti 2009 for the covers C1 and C2.

    score = 1 - mean([entropy(c1, 'given', c2), ...
                      entropy(c2, 'given', c1)]);
end
