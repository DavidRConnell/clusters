function A = getAdj(arg)
    if isstring(arg) || ischar(arg)
        arg = gread(arg);
    end

    ismatrix = size(arg, 1) == size(arg, 2);

    if ~ismatrix
        A = list2matrix(arg);
    else
        A = arg;
    end

    if isequal(A, triu(A))
        A = A + tril(A', -1);
    elseif isequal(A, tril(A))
        A = A + triu(A', 1);
    end
end

function A = list2matrix(arg)
    if size(arg, 2) == 2
        arg = [arg ones(size(arg, 1), 1)];
    end

    if min(arg(:, 1:2), [], 'all') < 1
        arg(:, 1:2) = arg(:, 1:2) - min(arg(:, 1:2), [], 'all') + 1;
    end
    nNodes = max(arg(:, 1:2), [], 'all');

    A = sparse(arg(:, 1), arg(:, 2), arg(:, 3), nNodes, nNodes);
end
