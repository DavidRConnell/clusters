function c = leiden(A, varargin)
%LEIDEN community detection
%   LEIDEN(A) perform leiden.
%   LEIDEN(A, ..., "initialMembership", membership) provide an initial
%       membership. Defaults to a singleton partition (one where every node is
%       in it's own community).
%   LEIDEN(A, ..., "partitionType", type) the quality function to use for
%       optimizing partitions.
%   LEIDEN(A, ..., "nIterations", i) how many iterations to perform. Should be
%       a positive integer or -1 (default). If -1 it will continue until
%       stable.
%   LEIDEN(A, ..., "resolution", gamma) resolution parameter to use (see CPM).
%   LEIDEN(A, ..., "beta", beta) randomness used in refinement stage.
%   LEIDEN(A, ..., "seed", seed) random seed to use if reprehensibility is
%       needed. By default a seed is chosen at random.

    assert(issymmetric(A), 'Adjacency matrix must be undirected.');

    p = inputParser();
    allowedPartitionTypes = {'modularity', 'cpm'};
    testParameter = @(x, allowedValues) any(validatestring(x, allowedValues));
    p.KeepUnmatched = true;
    p.addParameter('initialMembership', 0:(length(A) - 1), @(x) length(x) == length(A));
    p.addParameter('partitionType', 'modularity', @(x) testParameter(x, allowedPartitionTypes));
    p.addParameter('nIterations', -1, @(x) isnumeric(x) && x >= -1);
    p.addParameter('resolution', 1, @(x) isnumeric(x) && x > 0);
    p.addParameter('beta', 0.01, @(x) isnumeric(x) && x >= 0 && x <= 1);
    p.addParameter('seed', randi(10000, 1), @isnumeric);
    p.parse(varargin{:});

    if size(p.Results.initialMembership, 2) > 1
        initialMembership = p.Results.initialMembership';
    else
        initialMembership = p.Results.initialMembership;
    end

    if min(initialMembership) == 1
        initialMembership = initialMembership - 1;
    end

    assert(size(initialMembership, 2) == 1, 'initialMembership must be a vector.');

    c = leidenMex(A, ...
                  initialMembership, ...
                  p.Results.nIterations, ...
                  p.Results.resolution, ...
                  p.Results.beta, ...
                  p.Results.seed, ...
                  p.Results.partitionType);

    c = c + 1; % 0 index -> 1 index
end