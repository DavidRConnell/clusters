function edgesPairwise = countEdges(belongingCoef, A, belongingType)
    switch belongingType
      case 'product'
        belongingFunc = @productBelongingFunc;
      case 'average'
        belongingFunc = @averageBelongingFunc;
    end

    nCommunities = size(belongingCoef, 2);
    edgesPairwise = zeros(nCommunities, nCommunities);
    belongingFunc = @(c1, c2) ...
        belongingFunc(A, belongingCoef(:, c1), belongingCoef(:, c2));

    isASym = issymmetric(A);
    [c1, c2] = meshgrid(1:nCommunities);
    if isASym
        trilIndices = c2 <= c1;
        communityIndex = [c1(trilIndices) c2(trilIndices)]';
    else
        flatten = @(c) reshape(c, [1 numel(c1)]);
        communityIndex = [flatten(c1); flatten(c2)];
    end

    pairwiseEdgesList = zeros(size(communityIndex, 2), 1);
    parfor i = 1:numel(pairwiseEdgesList)
        pairwiseEdgesList(i) = belongingFunc(communityIndex(1, i), ...
                                             communityIndex(2, i));
    end

    linear2squareIndex = communityIndex(1, :) + ...
        ((communityIndex(2, :) - 1) * nCommunities);

    edgesPairwise(linear2squareIndex) = pairwiseEdgesList;

    if isASym
        edgesPairwise = tril2sym(edgesPairwise);
    end
end

function belonging = productBelongingFunc(A, coef1, coef2)
    belonging = sum((coef1 * coef2') .* A, 'all');
end

function belonging = averageBelongingFunc(A, coef1, coef2)
    belonging = coef1 + coef2';
    % Remove when node isn't in both of the communities.
    belonging(coef1 * coef2' == 0) = 0;
    belonging = sum(belonging .* A, 'all') / 2;
end

function sym = tril2sym(mat)
% Convert a lower triangle matrix to a symmetric one

    sym = mat + triu(mat', 1);
end