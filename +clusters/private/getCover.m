function c = getCover(arg)
%GETCOVER convert argument to a sparse cover
%   C = GETCOVER(ARG) determine whether ARG is a 2 column list of
%   nodes and community lables or a cover. If ARG is a cover return it
%   otherwise convert it to a cover first and return the cover.

    if isstring(arg) || ischar(arg)
        arg = gread(arg);
    end

    if size(arg, 2) > size(arg, 1)
        arg = arg';
    end

    if (size(arg, 2) == 1) && (max(arg, [], 'all') > 1)
        arg = [(1:length(arg))' arg];
    end

    if min(arg(:, 1)) < 1
        arg(:, 1) = arg(:, 1) - min(arg(:, 1)) + 1;
    end

    if (size(arg, 2) == 2) && (max(arg, [], 'all') > 1)
        c = spconvert([arg ones(length(arg), 1)]);
    elseif issparse(arg)
        c = arg;
    else
        c = sparse(arg);
    end

    if sum(sum(c, 1) == 0) > 0
        warning('MATLAB:getCover:EmptyCommunity', ...
                ['There are communities with no nodes assigned to them. ', ...
                'Deleting empty communities.'])
        c = c(:, sum(c, 1) ~= 0);
    end

    if max(c, [], 'all') > 1
        warning('MATLAB:getCover:MultipleAssignments', ...
                ['Some nodes wore assigned to the same community multiple times. ', ...
                'Ignoring extra assignments.'])
        c = double(c > 0);
    end
    c = c(:, randperm(size(c, 2)));
end
