function c = readGroup(file)
    fid = fopen(file, 'r');
    c = zeros(predictValues(file), 2) - 1;
    community = 0;
    count = 1;
    while ~feof(fid)
        line = fgetl(fid);
        community = community + 1;
        members = str2double(split(strip(line)));
        nMembers = length(members);
        c(count:(count + nMembers - 1), :) = [members repmat(community, [nMembers 1])];
        count = count + nMembers;
    end

    c = c(c(:, 1) >= 0, :);
    fclose(fid);
end

function n = predictValues(file)
% Assume < 1000 communities then ~3 digits + 1 space per number =
% 4 bytes / number. Round down to 3 to overestimate.

    bytes = dir(file).bytes;
    n = ceil(bytes / 3);
end