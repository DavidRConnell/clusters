function score = similarity(c1, c2, varargin)
%SIMILARITY compare two covers
%  SCORE = SIMILARITY(C1, C2) compare the covers C1 and C2. Useful for
%  testing community detection algorthims against the known partition
%  map or a gold standard method.
%
%  SCORE = SIMILARITY(C1, C2, 'TYPE', TYPE) use TYPE 'nmi' for
%  normalized mutual information (default), or 'omegaIndex'.

    c1 = getCover(c1);
    c2 = getCover(c2);

    missingNodes = size(c1, 1) - size(c2, 1);
    if missingNodes ~= 0
        warning('MATLAB:similarity:MissingNodes', ...
                ['Covers have different numbers of nodes. ', ...
                'Assuming the last nodes were not assigned to a community.']);

        if missingNodes > 0
            c2 = [c2; zeros(abs(missingNodes), size(c2, 2))];
        else
            c1 = [c1; zeros(abs(missingNodes), size(c1, 2))];
        end
    end

    p = inputParser;
    allowedTypes = {'nmi', 'omegaIndex'};
    testParameter = @(x, allowedValues) any(validatestring(x, allowedValues));
    p.addParameter('type', 'nmi', @(x) testParameter(x, allowedTypes));
    p.parse(varargin{:});

    switch validatestring(p.Results.type, allowedTypes)
      case 'nmi'
        score = nmi(c1, c2);
      case 'omegaIndex'
        score = omegaIndex(c1, c2);
    end
end