function A = list2Adj(list)
    A = spconvert([list ones(length(list), 1)]);
end