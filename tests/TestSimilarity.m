classdef TestSimilarity < matlab.unittest.TestCase
    properties (Access = private, Constant)
        nSimulations = 10;
        nNodes = 1000;
        nCommunities = 6;
        overlap = 1.2;
    end

    properties (TestParameter)
        type = struct('nmi', 'nmi', ...
                      'omegaIndex', 'omegaIndex');
    end

    methods (TestClassSetup)
        function recompileMex(obj)
            clusters.make();
        end
    end

    methods (Access = private)
        function c = generateData(obj)
            overlap = obj.overlap;
            c = zeros(obj.nNodes, obj.nCommunities);

            % Assume all nodes should be a member of atleast one class.
            if overlap > 1
                c(:, 1) = ones(obj.nNodes, 1);
                overlap = overlap - 1;
            end

            extraMembership = ones(ceil(obj.nNodes * overlap), 1);
            c(1:length(extraMembership), 2) = extraMembership;

            c = shuffle(c, 1);
            c = shuffle(c, 2);

            function c = shuffle(c, dim)
                m = size(c, dim);

                if dim == 1
                    idx = randperm(m);
                    c = c(idx, :);
                elseif dim == 2
                    for i = 1:size(c, 1)
                        idx = randperm(m);
                        c(i, :) = c(i, idx);
                    end
                end
            end
        end

        function verifyApprox(obj, actual, expected)
            if length(expected) == 1 && length(actual) > 1
                expected = repmat(expected, size(actual));
            end
            obj.verifyEqual(actual, expected, 'AbsTol', 1e-3);
        end

        function c2 = shuffleCommunities(obj, c1)
            idx = randperm(obj.nCommunities);
            c2 = c1(:, idx);
        end
    end

    methods (Test, TestTags = {'Unit', 'output'})
        function testEqualCovers(testCase, type)
            actual = zeros(1, testCase.nSimulations);
            for i = 1:testCase.nSimulations
                c = testCase.generateData();
                actual(i) = clusters.similarity(c, c, 'type', type);
            end

            expected = 1;
            testCase.verifyApprox(actual, expected);
        end

        function testArgumentOrder(testCase, type)
            expected = zeros(1, testCase.nSimulations);
            actual = zeros(1, testCase.nSimulations);
            for i = 1:testCase.nSimulations
                c1 = testCase.generateData();
                c2 = testCase.generateData();

                expected(i) = clusters.similarity(c1, c2, 'type', type);
                actual(i) = clusters.similarity(c2, c1, 'type', type);
            end

            testCase.verifyApprox(actual, expected);
        end

        function testCommunityOrderEqualCovers(testCase, type)
            actual = zeros(1, testCase.nSimulations);
            for i = 1:testCase.nSimulations
                c = testCase.generateData();
                cReordered = testCase.shuffleCommunities(c);
                actual(i) = clusters.similarity(c, cReordered, 'type', type);
            end

            expected = 1;
            testCase.verifyApprox(actual, expected);
        end

        function testCommunityOrderDifferentCovers(testCase, type)
            expected = zeros(1, testCase.nSimulations);
            actual = zeros(1, testCase.nSimulations);
            for i = 1:testCase.nSimulations
                c1 = testCase.generateData();
                c2 = testCase.generateData();
                c2Reordered = testCase.shuffleCommunities(c2);
                expected(i) = clusters.similarity(c1, c2, 'type', type);
                actual(i) = clusters.similarity(c1, c2Reordered, 'type', type);
            end

            testCase.verifyApprox(actual, expected);
        end

        function testSimilarity(testCase, type)
            smallChange = ceil(testCase.nNodes / 100);
            bigChange = ceil(testCase.nNodes / 10);
            actual = zeros(1, testCase.nSimulations);
            for i = 1:testCase.nSimulations
                c1 = testCase.generateData();
                c2 = c1;
                c4 = testCase.generateData();
                c2(1:smallChange, :) = c4(randi(testCase.nNodes, [1 smallChange]), :);
                c3 = c2;
                c3((smallChange + 1):bigChange, :) = c4(randi(testCase.nNodes, [1 (bigChange - smallChange)]), :);
                actual(i) = issorted(...
                    [clusters.similarity(c1, c1, 'type', type)
                     clusters.similarity(c1, c2, 'type', type)
                     clusters.similarity(c1, c3, 'type', type)
                     clusters.similarity(c1, c4, 'type', type)], ...
                    'strictdescend');
            end

            expected = 1;
            testCase.verifyApprox(actual, expected);
        end
    end

    methods (Test, TestTags = {'Unit', 'input'})
        function testCoverMissingNodes(testCase)
            c1 = [(1:testCase.nNodes)' randi(testCase.nCommunities, [testCase.nNodes 1])];
            c2 = c1(randperm(testCase.nNodes, testCase.nNodes - 5), :);

            testCase.verifyGreaterThan(clusters.similarity(c1, c2), 0);
        end

        function testLastNodeMissing(testCase)
            c1 = [(1:testCase.nNodes)' randi(testCase.nCommunities, [testCase.nNodes 1])];
            c2 = c1(1:(size(c1, 1) - 1), :);
            func = @() clusters.similarity(c1, c2);

            testCase.verifyWarning(func, 'MATLAB:similarity:MissingNodes');
            testCase.verifyGreaterThan(func(), 0);
        end

        function testCoverZeroIndexed(testCase)
            c1 = [(1:testCase.nNodes)' randi(testCase.nCommunities, [testCase.nNodes 1])];
            c2 = c1;
            c2(:, 1) = c2(:, 1) - 1;

            testCase.verifyEqual(clusters.similarity(c1, c2), 1);
        end

        function testCoverFromFile(testCase)
            c1 = '../data/football_disjoint_community.groups';
            c2 = '../data/football_true_community.groups';

            testCase.verifyGreaterThan(clusters.similarity(c1, c2), 0);
        end

        function testDifferentNumberOfCommunities(testCase, type)
            c1 = [(1:testCase.nNodes)' randi(testCase.nCommunities, [testCase.nNodes 1])];
            c2 = [(1:testCase.nNodes)' randi(testCase.nCommunities + 2, [testCase.nNodes 1])];

            testCase.verifyGreaterThan(clusters.similarity(c1, c2, 'type', type), 0);
        end
    end

    methods (Test, TestTags = {'Regression'})
        function TestKarateNetwork(testCase, type)
            rng(1);

            A = list2Adj(csvread('../data/karate_edges.csv', 1));

            cTrue = csvread('../data/karate_labels.csv', 1);
            cPredicted = slpa(A, 0.01, 20);

            actual = clusters.similarity(cTrue, cPredicted, 'type', type);
            cache = ['cache/karateresults' type '.txt'];
            if exist(cache, 'file') == 2
                fid = fopen(cache, 'r');
                expected = fscanf(fid, "%f");
                fclose(fid);
                testCase.verifyEqual(actual, expected, 'AbsTol', 10 ^ -5);
            else
                disp('Creating cache. Nothing to test against.')
                fid = fopen(cache, 'w');
                fprintf(fid, '%f', actual);
                fclose(fid);
                testCase.verifyTrue(true);
            end
        end
    end
end
