classdef TestModularity < matlab.unittest.TestCase
    properties (Access = private)
        A = csvread('../data/karate_edges.csv', 1);
        chenResults = struct('newman', ...
                             struct('product', [0.2873 0.3073 0.3159 0.3277 0.348 0.3635 0.3665 0.3693 0.3744 0.3787 0.3785], ...
                                    'average', [0.1247 0.1791 0.2386 0.269 0.2815 0.3199 0.3368 0.3542 0.3676 0.3751 0.3785]), ...
                             'density', ...
                             struct('product', [0.1338 0.1467 0.1593 0.166 0.1726 0.1844 0.1873 0.1863 0.1898 0.1948 0.1946], ...
                                    'average', [0.1212 0.1389 0.1528 0.1606 0.1687 0.1822 0.185 0.1814 0.1878 0.1939 0.1946]));
    end

    properties (TestParameter)
        type = struct('newman', 'newman', ...
                      'density', 'density');
        belonging = struct('product', 'product', ...
                           'average', 'average');
        % useMex = struct('M', false,'MEX', true);
        useMex = struct('MEX', true);
    end

    properties (ClassSetupParameter)
        useSparseAdj = struct('sparse', true, 'dense', false);
    end

    methods (TestClassSetup)
        function recompileMex(obj)
            clusters.make();
        end

        function convertAdjacencyStorage(obj, useSparseAdj)
            if ~useSparseAdj
                obj.A = full(obj.A);
            end
        end
    end


    methods (Access = private)
        function [A, c] = blockAdjacency(obj, nClusters, useSparseAdj)
            clusterSizes = randi([3 9], [1 nClusters]);
            edges = cell([1 nClusters]);
            c = zeros(sum(clusterSizes), nClusters);
            count = [1 1];
            for cluster = 1:nClusters
                edges{cluster} = ones(clusterSizes(cluster), 'logical');

                count(2) = count(1) + clusterSizes(cluster);
                c(count(1):(count(2) - 1), cluster) = 1;
                count(1) = count(2);
            end
            A = blkdiag(edges{:});
            if useSparseAdj
                A = sparse(A);
            end
        end

        function A = randomizeAdjacency(obj, A, pIn)
        % For simplicity.
            pOut = 1 - pIn;
            probs = (A .* pIn) + (~A .* pOut);
            A = probs > rand(size(A));
        end
    end

    methods (Test, TestTags = {'Unit', 'output'})
        function testIncreasedMixingReducesQ(testCase, type, belonging, useSparseAdj, useMex)
            nClusters = 6;
            [blockA, c] = testCase.blockAdjacency(nClusters, useSparseAdj);
            pIn = 1:-0.1:0.5;
            q = zeros(1, length(pIn));
            for i = 1:length(pIn)
                A = testCase.randomizeAdjacency(blockA, pIn(i));
                q(i) = clusters.modularity(A, c, ...
                                           'type', type, ...
                                           'belonging', belonging, ...
                                           'useMex', useMex);
            end

            testCase.verifyTrue(issorted(q, 'strictdescend'));
        end

        function testAgainstChen(testCase, type, belonging, useMex)
            testCase.assumeFail();
            r = [0.01, 0.05:0.05:0.5];
            T = 500;
            nSims = 10;
            q = zeros(nSims, length(r));
            for i = 1:length(r)
                for t = 1:nSims
                    c = slpa(testCase.A, r(i), T);
                    q(t, i) = clusters.modularity(testCase.A, c, ...
                                                  'type', type, ...
                                                  'belonging', belonging, ...
                                                  'useMex', useMex);
                end
            end
            expected = testCase.chenResults.(type).(belonging);
            tStatistic = (mean(q, 1) - expected) ./ (std(q, [], 1) / sqrt(nSims));
            alpha = 0.05;
            alpha = alpha / nSims;
            tCrit = tinv(1 - (alpha / 2), nSims - 1);
            testCase.verifyEqual(abs(tStatistic) < tCrit, ones(1, length(r), 'logical'));
        end

        function testSingleCommunityIsZero(testCase, type, belonging, useMex)
            if strcmpi(type, 'density')
                % Density algorithm known not to be zero for single community.
                testCase.assumeFail();
            end

            c = ones(length(testCase.A), 1);
            expected = 0;
            q = clusters.modularity(testCase.A, c, ...
                                    'type', type, ...
                                    'belonging', belonging, ...
                                    'useMex', useMex);
            testCase.verifyEqual(q, expected);
        end

        function testBothTypeOption(testCase, belonging, useMex)
            c = slpa(list2Adj(testCase.A), rand(1), 10);
            modularityWrapper = @(type) ...
                clusters.modularity(testCase.A, c, ...
                                    'type', type, ...
                                    'belonging', belonging, ...
                                    'useMex', useMex);

            both = modularityWrapper('both');
            newman = modularityWrapper('newman');
            density = modularityWrapper('density');
            testCase.verifyTrue((both(1) == newman) && (both(2) == density));
        end
    end

    methods (Test, TestTags = {'Unit', 'input'})
        function testCoverMissingNodes(testCase)
            nNodes = 1000;
            nCommunities = 6;
            A = sprand(nNodes, nNodes, 0.2) > 0.5;
            c = [(1:nNodes)' randi(nCommunities, [nNodes 1])];
            c = c(randperm(nNodes, nNodes - 5), :);

            testCase.verifyInstanceOf(clusters.modularity(testCase.A, c), 'double');
        end

        function testAdjZeroIndexed(testCase)
            nNodes = 1000;
            nCommunities = 6;
            A = [(1:nNodes)' randi(nNodes, [nNodes 1])] - 1;
            c = [(1:nNodes)' randi(nCommunities, [nNodes 1])];

            testCase.verifyInstanceOf(clusters.modularity(A, c), 'double');
        end

        function testAdjFromFile(testCase)
            A = '../data/karate_edges.csv';
            c = '../data/karate_labels.csv';

            testCase.verifyGreaterThan(clusters.modularity(A, c), 0);
        end

        function testSymmetricAdj(testCase)
        % Test when only half a symmetric Adj is supplied.
            A = '../data/football.txt';
            c = '../data/football_disjoint_community.groups';

            testCase.verifyGreaterThan(clusters.modularity(A, c), 0);
        end
    end

    methods (Test, TestTags = {'Regression'})
        function testEquivalenceAcrossMethods(testCase, type, belonging, useMex)
            rng(1);

            T = 10;
            r = [0.01, 0.05:0.05:0.5];
            actual = arrayfun(@modularityWrapper, r);

            cache = ['cache/modularityresults' belonging type '.txt'];
            if exist(cache, 'file') == 2
                fid = fopen(cache, 'r');
                expected = fscanf(fid, '%f')';
                fclose(fid);
                testCase.verifyEqual(actual, expected, 'AbsTol', 10 ^ -5);
            else
                disp('Creating cache. Nothing to test against.')
                fid = fopen(cache, 'w');
                fprintf(fid, '%f\n', actual);
                fclose(fid);
                testCase.verifyTrue(true);
            end

            function q = modularityWrapper(r)
                c = slpa(list2Adj(testCase.A), r, T);
                q = clusters.modularity(testCase.A, c, ...
                                        'type', type, ...
                                        'belonging', belonging, ...
                                        'useMex', useMex);
            end
        end
    end
end