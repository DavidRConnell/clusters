function plotPerformance(results, suite)
    resultsTable = scanResults(results, suite);
    prows = 'adjDensity';
    pcols = 'nCommunities';
    xvar = 'networkSize';
    yvar = 'time';
    performanceSubplots(resultsTable, xvar, yvar, prows, pcols);
end

function resultsTable = scanResults(results, suite)
    nParameterCombinations = length(suite);
    nParameters = length(suite(1).Parameterization);
    parameterNames = cell(1, ...
                          nParameters + 1);
    values = cell(nParameterCombinations, nParameters + 1);

    for i = 1:nParameters
        parameterNames{i} = suite(1).Parameterization(i).Property;
    end
    parameterNames{nParameters + 1} = 'time';

    for testi = 1:nParameterCombinations
        for pari = 1:nParameters
            values{testi, pari} = suite(testi).Parameterization(pari).Value;
        end

        values{testi, nParameters + 1} = mean(results(testi).Samples.MeasuredTime);
    end
    resultsTable = cell2table(values, 'VariableNames', parameterNames);
end

function performanceSubplots(tab, xvar, yvar, prows, pcols)
    nTests = height(tab);
    xValues = unique(tab.(xvar));
    rowValues = unique(tab.(prows));
    colValues = unique(tab.(pcols));
    nvals = length(xValues);
    nrows = length(rowValues);
    ncols = length(colValues);

    tab = sortrows(tab, xvar);
    nPlots = nrows * ncols;
    nSeries = nTests / (nPlots * nvals);
    for i = 1:nPlots
        ax(i) = subplot(nrows, ncols, i);
        if mod(i - 1, ncols) == 0
            ylabel('Mean time (s)');
            row = 1 + ((i - 1) / ncols);
            text(-0.5, 0.5, num2str(rowValues(row)), ...
                 'Units', 'Normalized', ...
                 'HorizontalAlignment', 'center');
            if row == 2
                text(-0.75, 0.5, "Adjacency Density", ...
                     'Units', 'Normalized', ...
                     'FontWeight', 'bold', ...
                     'FontSize', 16, ...
                     'Rotation', 90, ...
                     'HorizontalAlignment', 'center');
            end
        end
        if i > ((nrows - 1) * ncols)
            xlabel('Number of nodes')
            col = i - ((nrows - 1) * ncols);
            text(0.5, -0.25, num2str(colValues(col)), ...
                 'Units', 'Normalized', ...
                 'HorizontalAlignment', 'center');
            if col == 2
                text(1.25, -0.35, 'Number of Communities', ...
                     'Units', 'Normalized', ...
                     'FontWeight', 'bold', ...
                     'FontSize', 16, ...
                     'HorizontalAlignment', 'center');
            end
        end
        if i == ncols
            addLegend();
        end

        hold on
        for j = 1:nSeries
            offset = (i - 1) * nSeries;
            rng = (0:(nvals - 1)) * (nPlots * nSeries);
            data = tab(j + offset + rng, :);
            if data.useMex
                color = 'blue';
            else
                color = 'red';
            end

            if strcmp(data.belonging, 'product')
                marker = '*';
            else
                marker = '+';
            end

            if data.useSparseAdj
                lineStyle = ':';
            else
                lineStyle = '-';
            end
            semilogx(data.(xvar), data.(yvar), ...
                 'Color', color, ...
                 'Marker', marker, ...
                 'LineStyle', lineStyle)
        end
        hold off
    end

    linkaxes(ax, 'xy');

    function addLegend()
        x = 1.05;
        y = 0.95;

        addChild({'Use MEX'}, ...
                 {'true -', 'Color', 'blue'}, ...
                 {'false -', 'Color', 'red'});
        addChild({'Belonging'}, ...
                 {'product *'}, {'average +'});
        addChild({'Sparse Adj'}, ...
                 {'true -'}, {'false :'});

        function addChild(title, varargin)
            titleShift = 0.09;
            elementShift = 0.07;
            childShift = 0.13;

            text(x, y, title{:}, 'FontWeight', 'bold', 'Units', 'Normalized')
            y = y - titleShift;

            for element = varargin
                name = element{1}{1};
                opts = element{1}(2:end);
                text(x + 0.025, y, name, opts{:}, 'FontSize', 10, 'Units', 'Normalized');
                y = y - elementShift;
            end

            y = y - childShift;
        end
    end
end