import matlab.perftest.TimeExperiment
testSuite = matlab.unittest.TestSuite.fromFile('TestModularityPerformance.m');
experiment = TimeExperiment.limitingSamplingError(...
    'NumWarmups', 2, ...
    'MinSamples', 3, ...
    'MaxSamples', 15);

results = run(experiment, testSuite);

plotPerformance(results, testSuite);