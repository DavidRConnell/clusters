classdef TestModularity < matlab.perftest.TestCase
    properties (TestParameter)
        belonging = struct('product', 'product', 'average', 'average');
        useMex = struct('M', false, 'MEX', true);
        numThreads = num2cell(1:4);
    end

    properties (MethodSetupParameter)
        useSparseAdj = struct('sparse', true, 'dense', false);
        networkSize = num2cell(floor(logspace(1, 3, 10)));
        adjDensity = num2cell(linspace(0.1, 1, 3));
        nCommunities = num2cell(floor(logspace(log10(5), log10(100), 4)));
    end

    properties (Access = private)
        A {double}
        c {double}
        type = 'newman';
    end

    methods (TestClassSetup)
        function recompileMex(obj)
            clusters.make();
        end
    end

    methods (TestMethodSetup)
        function generateAdjMatrix(testCase, networkSize, adjDensity, useSparseAdj, nCommunities)
            testCase.A = sprand(networkSize, networkSize, adjDensity) > 0;
            if ~useSparseAdj
                testCase.A = full(testCase.A);
            end

            testCase.c = [(1:networkSize)', randi(nCommunities, [networkSize, 1])];
        end
    end

    methods (Test, TestTags = {'Performance'})
        function testEdgeCountingPerformance(testCase, belonging, useMex)
            if strcmp(belonging, 'average') && ~useMex
                % M average is very slow for large networks.
                testCase.assumeFail();
            end
            testCase.startMeasuring();
            clusters.modularity(testCase.A, testCase.c, ...
                                'type', testCase.type, ...
                                'belonging', belonging, ...
                                'useMex', useMex);
            testCase.stopMeasuring();
        end

        function testParallelPerformance(testCase, numThreads)
            setenv('OMP_NUM_THREADS', num2str(numThreads));
            testCase.startMeasuring();
            clusters.modularity(testCase.A, testCase.c, ...
                                'type', testCase.type, ...
                                'belonging', 'product');
            testCase.stopMeasuring();
        end
    end
end
