/*===================================================================
 * countUnique.c - Count instances of unique values
 *
 * Intended to be called inside of omegaIndex.m and not directly.
 * Inputs:
 *       x: a sparse column vector.
 *       k: max value to count.
 *
 * Counts values from 0:k. Returning a k+1 row vector with the results.
 *
 * Call with:
 *
 *         y = countUnique(x, k);
 *
 * Build using clusters.make() in matlab.
 *===================================================================*/

#include <mex.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  double *x;
  mwIndex *x_rows, *x_cols;
  mwSize n;
  mwSize k;
  double *y;

  if (!(nrhs == 2)) {
    mexErrMsgIdAndTxt("Clusters:countUnique:nrhs", "Must have two inputs.");
  }

  if (!mxIsDouble(prhs[0])) {
    mexErrMsgIdAndTxt("Clusters:countUnique:notDouble",
                      "Matrix must be a double.");
  }

  if (!mxIsSparse(prhs[0])) {
    mexErrMsgIdAndTxt("Clusters:countUnique:notSparse", "Input must be sparse.");
  }

  if (!(mxGetN(prhs[0]) == 1)) {
    mexErrMsgIdAndTxt("Clusters:countUnique:notColumn",
                      "Input must be column vector.");
  }

  n = mxGetM(prhs[0]);
  k = mxGetScalar(prhs[1]);
  x = mxGetPr(prhs[0]);
  x_rows = mxGetIr(prhs[0]);
  x_cols = mxGetJc(prhs[0]);
  mwIndex numel = x_cols[1];

  plhs[0] = mxCreateDoubleMatrix(k + 1, 1, mxREAL);
  y = mxGetPr(plhs[0]);
  y[0] = n - numel;

  mwIndex i;
  for (i = 0; i < numel; i++) {
    if (x[i] <= k) {
      y[(mwIndex)x[i]]++;
    }
  }
}
