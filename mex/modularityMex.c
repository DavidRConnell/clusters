/*===================================================================
 * modularityMex.c - Calculate modularity for a network
 *
 * Intended to be called inside of modularity.m and not directly.
 *
 * Call with:
 *         q = modularityMex(adj, cover, type, belonging)
 *
 * Build using clusters.make() in matlab.
 *===================================================================*/

#include "mex.h"
#include <string.h>

#define IN(i) i
#define OUT(i) (i + n_communities)
#define DC_DEN(i) (i + (n_communities * 2))
#define DCC(i) (i + (n_communities * 3))

#define STR_EQ(str1, str2) (strcmp(str1, str2) == 0)
#define COL_IDX(c, m) m->col_idx[c]
#define MAT_IDX(idx, m) m->val[idx]
#define ROW_IDX(idx, m) m->row_idx[idx]
#define DENSE_IDX(i, j, m) m->val[i + (j * m->n_rows)]

#define PRODUCT_BELONGING(ai, aj) ai *aj
#define AVERAGE_BELONGING(ai, aj) (ai + aj) / 2

int TYPE;

typedef struct {
  mwIndex *col_idx;
  mwIndex *row_idx;
  double *val;
  mwSize n_rows;
  mwSize n_columns;
  bool is_sparse;
} mat;

static void print_matrix(const mat *adj)
{
  mexPrintf("\n");
  for (int i = 0; i < adj->n_rows; i++) {
    for (int j = adj->col_idx[i]; j < adj->col_idx[i + 1]; j++) {
      mexPrintf("   (%d,%d)       %0.4f\n", adj->row_idx[j] + 1, i + 1,
                adj->val[j]);
    }
  }
  mexPrintf("\n");
}

static void dense_accumulator(const mat *adj, const mat *cover,
                              const double *coef, const mwSize c1,
                              const mwSize c2, double *edges,
                              double *edge_acc)
{
  double current_edge;
  mwSize n_communities = cover->n_rows;

  double Aij;
  mwSize i, j;
  for (i = COL_IDX(c1, cover); i < COL_IDX(c1 + 1, cover); i++) {
    for (j = COL_IDX(c2, cover); j < COL_IDX(c2 + 1, cover); j++) {
      if (TYPE) {
        current_edge =
          AVERAGE_BELONGING(coef[ROW_IDX(i, cover)], coef[ROW_IDX(j, cover)]);
      } else {
        current_edge =
          PRODUCT_BELONGING(coef[ROW_IDX(i, cover)], coef[ROW_IDX(j, cover)]);
      }
      Aij = DENSE_IDX(ROW_IDX(i, cover), ROW_IDX(j, cover), adj);
      if (Aij != 0)
        edge_acc[0] += current_edge * Aij;

      edge_acc[1] += current_edge;

      if (!(i == j) && (c1 == c2))
        edges[DC_DEN(c1)] += current_edge;
    }
  }
}

static void sparse_accumulator(const mat *adj, const mat *cover,
                               const double *coef, const mwSize c1,
                               const mwSize c2, double *edges, double *edge_acc)
{
  double current_edge;

  mwSize n_communities = cover->n_rows;
  mwSize n_idx, n_max;
  mwSize A_row, A_col;
  mwSize i, j;
  // Flip c1 and c2 so c2 can still represent the column number of Adj.
  for (j = COL_IDX(c2, cover); j < COL_IDX(c2 + 1, cover); j++) {
    A_col = ROW_IDX(j, cover);
    n_idx = COL_IDX(A_col, adj);
    n_max = COL_IDX(A_col + 1, adj);
    for (i = COL_IDX(c1, cover); i < COL_IDX(c1 + 1, cover); i++) {
      A_row = ROW_IDX(i, cover);
      if (TYPE) {
        current_edge =
          AVERAGE_BELONGING(coef[ROW_IDX(i, cover)], coef[ROW_IDX(j, cover)]);
      } else {
        current_edge =
          PRODUCT_BELONGING(coef[ROW_IDX(i, cover)], coef[ROW_IDX(j, cover)]);
      }
      while ((ROW_IDX(n_idx, adj) < A_row) && (n_idx < (n_max - 1)))
        n_idx += 1;

      if (ROW_IDX(n_idx, adj) == A_row)
        edge_acc[0] += current_edge * MAT_IDX(n_idx, adj);

      edge_acc[1] += current_edge;
      /* Really if not ROW_IDX(i, cover) == ROW_IDX(j, cover) but becuse
      it only applies when c1 == c2, when row indices are the same so
      are i and j. */
      if (!(i == j) && (c1 == c2))
        edges[DC_DEN(c1)] += current_edge;
    }
  }
}

static void count_edges(const mat *adj, const mat *cover, const double *coef,
                        double *edges)
{
  void (*accumulator)(const mat *, const mat *, const double *, const mwSize,
                      mwSize, double *, double *);
  double edge_acc[2];
  if (adj->is_sparse) {
    accumulator = sparse_accumulator;
  } else {
    accumulator = dense_accumulator;
  }
  mwSize n_communities = cover->n_rows;

  mwSize c1, c2;
  #pragma omp parallel for private(c1, c2, edge_acc) collapse(2)
  for (c1 = 0; c1 < cover->n_rows; c1++) {
    for (c2 = 0; c2 < cover->n_rows; c2++) {
      edge_acc[0] = 0;
      edge_acc[1] = 0;
      accumulator(adj, cover, coef, c1, c2, edges, edge_acc);
      if (c1 == c2) {
        edges[IN(c1)] += edge_acc[0];
      } else {
        edges[OUT(c1)] += edge_acc[0];
        edges[DCC(c1)] += (edge_acc[0] * edge_acc[0]) / edge_acc[1];
      }
    }
  }
}

static void modularity(const double total_edges, const double *edges,
                       const mwSize n_communities, double *q)
{
  double edgesDc, term2;
  mwSize i;
  for (i = 0; i < n_communities; i++) {
    term2 = (edges[IN(i)] + edges[OUT(i)]) / total_edges;
    q[0] += (edges[IN(i)] / total_edges) - (term2 * term2);

    if (edges[DC_DEN(i)] == 0) {
      edgesDc = edges[IN(i)];
    } else {
      edgesDc = edges[IN(i)] / edges[DC_DEN(i)];
    }

    q[1] += (edges[IN(i)] * edgesDc) / total_edges;
    q[1] -= (term2 * edgesDc) * (term2 * edgesDc);
    q[1] -= edges[DCC(i)] / total_edges;
  }
}

static bool is_signed(const mat *adj)
{
  if (adj->is_sparse) {
    for (int i = 0; i < adj->col_idx[adj->n_rows]; i++) {
      if (MAT_IDX(i, adj) < 0) {
        return true;
      }
    }
  } else {
    for (int i = 0; i < adj->n_rows; i++) {
      for (int j = 0; j < adj->n_columns; j++) {
        if (DENSE_IDX(i, j, adj) < 0) {
          return true;
        }
      }
    }
  }

  return false;
}

static inline double total_edges(const mat *adj)
{
  double edge_acc = 0;
  if (adj->is_sparse) {
    for (mwSize i = 0; i < adj->col_idx[adj->n_rows]; i++) {
      edge_acc += adj->val[i];
    }
  } else {
    for (mwIndex i = 0; i < adj->n_rows; i++) {
      for (mwIndex j = 0; j < adj->n_columns; j++) {
        edge_acc += adj->val[i + (j * adj->n_rows)];
      }
    }
  }

  return edge_acc;
}

static void unsigned_modularity(const mat *adj, const mat *cover,
                                const double *coef, const double total_edges, double *q)
{
  /* edges in, out, dc_denom, dcc. */
  double *edges = mxCalloc(cover->n_rows * 4, sizeof(double));
  count_edges(adj, cover, coef, edges);

  modularity(total_edges, edges, cover->n_rows, q);
  mxFree(edges);
};

#define GUARD_AGAINST_EMPTY(n_elements)		\
  if ((n_elements) == 0) {			\
    continue;					\
  }

static void signed_modularity(const mat *adj, const mat *cover,
                              const double *coef, double *q)
{
  mwSize n_elements[2] = {0, 0};

  /* Assume if signed, both the positive and negative sets of weights will
  account for a non-negligable number of edges so both the positive and
  negative adj will benefit from being sparse. */
  if (adj->is_sparse) {
    for (int i = 0; i < adj->col_idx[adj->n_rows]; i++) {
      if (MAT_IDX(i, adj) > 0) {
        n_elements[0]++;
      } else { // NOTE: If sparse, can't == 0, so no need to check if < 0
        n_elements[1]++;
      }
    }
  } else {
    for (int i = 0; i < adj->n_rows; i++) {
      for (int j = 0; j < adj->n_columns; j++) {
        if (DENSE_IDX(i, j, adj) > 0) {
          n_elements[0]++;
        } else if (DENSE_IDX(i, j, adj) < 0) {
          n_elements[1]++;
        }
      }
    }
  }

  mwIndex *col_idx[2];
  mwIndex *row_idx[2];
  double *val[2];

  for (int is_negative = 0; is_negative < 2; is_negative++) {
    GUARD_AGAINST_EMPTY(n_elements[is_negative]);

    col_idx[is_negative] = mxMalloc(sizeof(mwIndex) * adj->n_rows);
    row_idx[is_negative] = mxMalloc(sizeof(mwIndex) * n_elements[is_negative]);
    val[is_negative] = mxMalloc(sizeof(double) * n_elements[is_negative]);
  }

  for (int is_negative = 0; is_negative < 2; is_negative++) {
    GUARD_AGAINST_EMPTY(n_elements[is_negative]);
    col_idx[is_negative][0] = 0;
  }

  mwIndex count[2] = {0, 0};
  mwIndex is_negative = 0;
  int multiplier = 0;
  if (adj->is_sparse) {
    for (int i = 0; i < adj->n_rows; i++) {
      for (int j = adj->col_idx[i]; j < adj->col_idx[i + 1]; j++) {
        if (MAT_IDX(j, adj) > 0) {
          is_negative = 0;
          multiplier = 1;
        } else {
          is_negative = 1;
          multiplier = -1;
        }
        row_idx[is_negative][count[is_negative]] = ROW_IDX(j, adj);
        val[is_negative][count[is_negative]] = MAT_IDX(j, adj) * multiplier;
        count[is_negative]++;
      }

      for (int is_negative = 0; is_negative < 2; is_negative++) {
        col_idx[is_negative][i + 1] = count[is_negative];
      }
    }
  } else {
    for (int j = 0; j < adj->n_columns; j++) {
      for (int i = 0; i < adj->n_rows; i++) {
        if (DENSE_IDX(i, j, adj) > 0) {
          is_negative = 0;
          multiplier = 1;
        } else if (DENSE_IDX(i, j, adj) < 0) {
          is_negative = 1;
          multiplier = -1;
        } else {
          continue;
        }

        row_idx[is_negative][count[is_negative]] = i;
        val[is_negative][count[is_negative]] = DENSE_IDX(i, j, adj) * multiplier;
        count[is_negative]++;
      }

      for (int is_negative = 0; is_negative < 2; is_negative++) {
        GUARD_AGAINST_EMPTY(n_elements[is_negative])
        col_idx[is_negative][j + 1] = count[is_negative];
      }
    }
  }

  double split_q[2][2];
  double split_edges[2];
  for (int is_negative = 0; is_negative < 2; is_negative++) {
    if (n_elements[is_negative] == 0) {
      split_q[is_negative][0] = 0;
      split_q[is_negative][1] = 0;
      split_edges[is_negative] = 0;
      continue;
    }

    mat split_adj = {
      col_idx[is_negative],
      row_idx[is_negative],
      val[is_negative],
      adj->n_rows,
      (mwSize)NULL,
      true
    };

    split_edges[is_negative] = total_edges(&split_adj);
    unsigned_modularity(&split_adj, cover, coef, split_edges[is_negative],
                        split_q[is_negative]);
  }

  for (int i = 0; i < 2; i++) {
    q[i] = (split_edges[0] * split_q[0][i]) - (split_edges[1] * split_q[1][i]);
    q[i] /= (split_edges[0] + split_edges[1]);
  }

  for (int i = 0; i < 2; i++) {
    GUARD_AGAINST_EMPTY(n_elements[i])
    mxFree(row_idx[i]);
    mxFree(val[i]);
  }
};

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  char *belonging;
  mwSize n_nodes;
  mwSize n_communities;

  if (nrhs != 3) {
    mexErrMsgIdAndTxt("Clusters:modularity:nrhs", "Three inputs required.");
  }

  if (mxIsComplex(prhs[0]) || mxIsComplex(prhs[1])) {
    mexErrMsgIdAndTxt(
      "Clusters:modularity:complexInputs",
      "Don't know how to handle complex adjacency matrices or covers.");
  }

  if (!(mxIsDouble(prhs[0]) && mxIsDouble(prhs[1]))) {
    mexErrMsgIdAndTxt("Clusters:modularity:notDouble",
                      "First two inputs must be doubles.");
  }

  if (!mxIsSparse(prhs[1])) {
    mexErrMsgIdAndTxt("Clusters:modularity:notSparse",
                      "Belonging coefficent must be sparse.");
  }

  if (!mxIsChar(prhs[2])) {
    mexErrMsgIdAndTxt("Clusters:modularity:notChar",
                      "Belonging must be a string.");
  }

  n_nodes = mxGetM(prhs[1]);
  n_communities = mxGetN(prhs[1]);

  mwIndex *col_idx;
  mwIndex *row_idx;
  double *val;
  mwSize n_rows;
  mwSize n_columns;
  bool is_sparse;

  if (mxIsSparse(prhs[0])) {
    col_idx = mxGetJc(prhs[0]);
    row_idx = mxGetIr(prhs[0]);
    val = mxGetPr(prhs[0]);
    n_rows = n_nodes;
    n_columns = (mwSize)NULL;
    is_sparse = true;
  } else {
    col_idx = (mwIndex *)NULL;
    row_idx = (mwIndex *)NULL;
    val = mxGetPr(prhs[0]);
    n_rows = n_nodes;
    n_columns = n_nodes;
    is_sparse = false;
  }

  mat adj = {col_idx, row_idx, val, n_rows, n_columns, is_sparse};
  mat cover = {mxGetJc(prhs[1]), mxGetIr(prhs[1]), mxGetPr(prhs[1]), n_communities, (mwSize)NULL, true};

  double *coef = mxCalloc(n_nodes, sizeof(double));
  for (mwIndex i = 0; i < cover.col_idx[n_communities]; i++) {
    coef[cover.row_idx[i]]++;
  }
  for (mwIndex i; i < n_nodes; i++) {
    coef[i] = 1 / coef[i];
  }

  belonging = mxArrayToString(prhs[2]);
  if (STR_EQ(belonging, "product")) {
    TYPE = 0;
  } else if (STR_EQ(belonging, "average")) {
    TYPE = 1;
  } else {
    mexErrMsgIdAndTxt("Clusters:modularity:invalidBelongingType",
                      "Belonging type must be one of 'product' or 'average'");
  }

  plhs[0] = mxCreateDoubleMatrix((mwSize)1, (mwSize)2, mxREAL);
  double *q = mxGetPr(plhs[0]);

  if (is_signed(&adj)) {
    signed_modularity(&adj, &cover, coef, q);
  } else {
    double all_edges = total_edges(&adj);
    unsigned_modularity(&adj, &cover, coef, all_edges,  q);
  }
  mxFree(coef);
}
