#include <mex.h>
#include <string.h>
#include <igraph.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if (nrhs != 7) {
    mexErrMsgIdAndTxt("Clusters:findPartition:nrhs", "Seven inputs required.");
  }

  if (mxIsComplex(prhs[0])) {
    mexErrMsgIdAndTxt(
      "Clusters:findPartition:complexInputs",
      "Don't know how to handle complex adjacency matrices.");
  }

  if (!(mxIsDouble(prhs[0]))) {
    mexErrMsgIdAndTxt("Clusters:findPartition:notDouble",
                      "First input must be double.");
  }

  if (!(mxIsDouble(prhs[1]))) {
    mexErrMsgIdAndTxt("Clusters:findPartition:notDouble",
                      "Second input must be doubles.");
  }

  if (!(mxIsDouble(prhs[2]) || mxIsDouble(prhs[3]) || mxIsDouble(prhs[4]) ||
        mxIsDouble(prhs[5]))) {
    mexErrMsgIdAndTxt("Clusters:findPartition:notDouble",
                      "Inputs 3 through 6 must be doubles.");
  }

  if (!(mxIsChar(prhs[6]))) {
    mexErrMsgIdAndTxt("Clusters:findPartition:notChar",
                      "Last input must be a character array.");
  }

  igraph_t graph;
  igraph_vector_t weights;
  igraph_vector_int_t edges;
  double *adj = mxGetPr(prhs[0]);
  mwSize n_nodes = mxGetN(prhs[0]);
  if (mxIsSparse(prhs[0])) {
    mwIndex *adj_col_idx = mxGetJc(prhs[0]);
    mwIndex *adj_raw_idx = mxGetIr(prhs[0]);
    mwSize n_edges = adj_col_idx[n_nodes];
    igraph_vector_int_init(&edges, 2 * n_edges);
    igraph_vector_init(&weights, n_edges);
    for (int i = 0; i < n_nodes; i++) {
      for (int j = adj_col_idx[i]; j < adj_col_idx[i + 1]; j++) {
        VECTOR(edges)[j * 2] = (igraph_integer_t)i;
        VECTOR(edges)[(j * 2) + 1] = (igraph_integer_t)adj_raw_idx[j];
        VECTOR(weights)[j] = (igraph_real_t)adj[j];
      }
    }
  } else {
    mwSize n_edges = 0;
    for (int i = 0; i < (n_nodes * n_nodes); i++) {
      n_edges += (adj[i] != (double)0);
    }
    igraph_vector_int_init(&edges, 2 * n_edges);
    igraph_vector_init(&weights, n_edges);
    int edge_n = 0;
    for (int i = 0; i < n_nodes; i++) {
      for (int j = 0; j < n_nodes; j++) {
        if (adj[i + (j * n_nodes)] != 0) {
          VECTOR(edges)[edge_n * 2] = (igraph_integer_t)j;
          VECTOR(edges)[(edge_n * 2) + 1] = (igraph_integer_t)i;
          VECTOR(weights)[edge_n] = (igraph_real_t)adj[i + (j * n_nodes)];
          edge_n++;
        }
      }
    }
  }
  igraph_create(&graph, &edges, 0, IGRAPH_UNDIRECTED);
  igraph_vector_int_destroy(&edges);

  double *initial_membership = mxGetPr(prhs[1]);
  igraph_vector_int_t membership;
  igraph_vector_int_init(&membership, n_nodes);
  for (int i = 0; i < n_nodes; i++) {
    VECTOR(membership)[i] = (igraph_integer_t)initial_membership[i];
  }

  igraph_integer_t n_iterations = (igraph_integer_t)mxGetScalar(prhs[2]);
  igraph_real_t resolution = (igraph_real_t)mxGetScalar(prhs[3]);
  igraph_real_t beta = (igraph_real_t)mxGetScalar(prhs[4]);

  igraph_integer_t seed = (igraph_integer_t)mxGetScalar(prhs[5]);
  igraph_rng_seed(igraph_rng_default(), seed);

  // Matlab wrapper function passes initial membership so always start
  igraph_bool_t start = true;

  char *partitionType = mxArrayToString(prhs[6]);
  if (strcmp(partitionType, "modularity") == 0) {
    igraph_vector_t strength;
    igraph_vector_init(&strength, n_nodes);
    igraph_strength(&graph, &strength, igraph_vss_all(), IGRAPH_ALL, 1,
                    &weights);
    igraph_real_t m;
    m = igraph_vector_sum(&weights);
    igraph_community_leiden(&graph, &weights, &strength, resolution / (2 * m),
                            beta, start, n_iterations, &membership, NULL, NULL);
  } else {
    igraph_community_leiden(&graph, &weights, NULL, resolution,
                            beta, start, n_iterations, &membership, NULL, NULL);
  }

  plhs[0] = mxCreateDoubleMatrix(n_nodes, 1, mxREAL);
  double *partition = mxGetPr(plhs[0]);
  for (int i = 0; i < n_nodes; i++) {
    partition[i] = (double)VECTOR(membership)[i];
  }

  igraph_vector_destroy(&weights);
  igraph_vector_int_destroy(&membership);
  igraph_destroy(&graph);
}
