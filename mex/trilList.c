/*===================================================================
 * trilList.c - Collect the elements in the lower triangle of a matrix
 *
 * Intended to be called inside of omegaIndex.m and not directly.
 * Inputs:
 *       x: square matrix.
 *       k: diagonal number. if absent defaults to 0.
 *
 * Call with:
 *
 *         y = trilList(x, k);
 *
 * Build using clusters.make() in matlab.
 * See also tril.
 *===================================================================*/

#include <mex.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  double *x;
  mwIndex *x_rows, *x_cols;
  mwIndex k;
  double *y;
  mwIndex *y_rows, *y_cols;
  mwSize n;
  mwIndex x_nnz, numel;

  if (nrhs > 2 || nrhs < 1) {
    mexErrMsgIdAndTxt("Clusters:trilList:nrhs", "Must have one or two inputs.");
  }

  if (!mxIsDouble(prhs[0])) {
    mexErrMsgIdAndTxt("Clusters:trilList:notDouble",
                      "Matrix must be a double.");
  }

  if (!mxIsSparse(prhs[0])) {
    mexErrMsgIdAndTxt("Clusters:trilList:notSparse", "Matrix must be sparse.");
  }

  if (!(mxGetN(prhs[0]) == mxGetM(prhs[0]))) {
    mexErrMsgIdAndTxt("Clusters:trilList:notSquare", "Matrix must be square.");
  }

  n = mxGetN(prhs[0]);
  x = mxGetPr(prhs[0]);
  x_rows = mxGetIr(prhs[0]);
  x_cols = mxGetJc(prhs[0]);
  x_nnz = x_cols[n];

  if (nrhs == 2) {
    k = -(mwIndex)mxGetScalar(prhs[1]);
  } else {
    k = 0;
  }

  numel = ((n - k) * (n - k + 1)) / 2;
  mwSize nnz = (x_nnz < numel ? x_nnz : numel);
  plhs[0] = mxCreateSparse((mwSize)numel, 1, nnz, mxREAL);
  y = mxGetPr(plhs[0]);
  y_rows = mxGetIr(plhs[0]);
  y_cols = mxGetJc(plhs[0]);

  mwIndex i, j;
  mwIndex count = 0;
  mwIndex col_min_idx = 0;
  for (i = 0; i < n; i++) {
    for (j = x_cols[i]; j < (x_cols[i + 1]); j++) {
      if (x_rows[j] >= (i + k)) {
        y_rows[count] = col_min_idx + x_rows[j] - (i + k);
        y[count] = x[j];
        count += 1;
      }
    }
    col_min_idx += (mwIndex)(n - k) - i;
  }
  y_cols[1] = count;
}
