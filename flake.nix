{
  description = "Matlab package for quantifying network clustering.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    nix-matlab = {
      url = "gitlab:doronbehar/nix-matlab";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    matlab-wrapper.url = "/home/voidee/packages/nixpkgs/matlabEnv";
  };

  outputs = { self, nixpkgs, nix-matlab, matlab-wrapper }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      clusters = matlab-wrapper.packages.${system}.mkPackage {
        pname = "clusters";
        version = "0.1.0";
        src = ./+clusters;
      };
    in {
      devShells.${system}.default = pkgs.mkShell {
        buildInputs = (with nix-matlab.packages.${system}; [
          matlab
          matlab-mlint
          matlab-mex
        ]) ++ [ pkgs.astyle pkgs.bear pkgs.gnumake pkgs.gdb pkgs.igraph ];
        shellHook = ''
          export OMP_NUM_THREADS=8
          export C_INCLUDE_PATH=${pkgs.igraph.dev}/include/igraph
          export LIBRARY_PATH=${pkgs.igraph}/lib
        '';
      };
      packages.clusters = clusters;
      packages.default = self.packages.${system}.clusters;
    };
}
