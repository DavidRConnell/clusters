%% Define data
A = 'data/football.txt';
cTrue = 'data/football_true_community.groups';
cPredicted = 'data/football_disjoint_community.groups';

%% Compare to values calculated from Chen's code.

clusters.similarity(cTrue, cPredicted, 'type', 'nmi')
% Expected = 0.8035

clusters.modularity(A, cPredicted, 'type', 'both')
% Expected = 0.6010 0.4887

%% Test community detection
resolution = 0.1;
for func = {'cpm', 'modularity'}
    cDetected = clusters.findPartition(A, 'type', 'leiden', ...
                                       'partitionType', func{1}, ...
                                       'resolution', resolution, ...
                                       'seed', 1234);
  clusters.similarity(cTrue, cDetected, 'type', 'nmi')
  clusters.modularity(A, cDetected, 'type', 'both')
end

cDetected = clusters.findPartition(A, 'type', 'louvain', ...
                                   'resolution', resolution, ...
                                   'seed', 1234);
clusters.modularity(A, cDetected, 'type', 'both')
