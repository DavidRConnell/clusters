%% Define data
A = 'data/LFR.txt';
cTrue = 'data/LFR_true_community.groups';
cPredicted = 'data/LFR_overlapping_community.groups';

%% Compare to values calculated from Chen's code.

clusters.similarity(cTrue, cPredicted, 'type', 'nmi')
% Expected = 0.6941

clusters.similarity(cTrue, cPredicted, 'type', 'omegaIndex')
% Expected = 0.5747

clusters.modularity(A, cPredicted, 'type', 'newman')
% Expected = 0.6179

clusters.modularity(A, cPredicted, 'type', 'density')
% Expected = 0.4473